module.exports = {
  //publicPath: '/tracecross/',
  configureWebpack: {
    module: {
      rules: [{
        test: /\.md$/,
        loader: 'raw-loader', // npm install -D raw-loader
      }]
    }
  }
}