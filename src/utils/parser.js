import XLSX from "xlsx";


export function getUsers(ws) {
    const table = getTable(ws);
    const pool = [];
    let sns_col = -1

    table.forEach(row => {
        if (sns_col < 0) {
            sns_col = findSNS(row)
        } else {
            pool.push(String(row[sns_col]).trim())
        }
    });
    return pool
}

function findSNS(row) {
    for (let i = 0; i < row.length; i++) {
        const el = String(row[i]).toLowerCase();
        if (el.indexOf('sns') >= 0 || el == 'utente') {
            return i
        }
    }
    return -1
}

function findName(row) {
    for (let i = 0; i < row.length; i++) {
        const el = String(row[i]).toLowerCase();
        if (el.indexOf('nome') >= 0) {
            return i
        }
    }
    return -1
}

export function getTable(ws) {
    const range = XLSX.utils.decode_range(ws['!ref']);
    const result = [];
    for (let rowNum = range.s.r; rowNum <= range.e.r; rowNum++) {
        const row = [];
        for (let colNum = range.s.c; colNum <= range.e.c; colNum++) {
            var nextCell = ws[
                XLSX.utils.encode_cell({ r: rowNum, c: colNum })
            ];
            if (typeof nextCell === 'undefined') {
                row.push(void 0);
            } else row.push(nextCell.w || nextCell.v);
        }
        result.push(row);
    }
    return result;
}

function findInPool(sns, pool) {
    for (const k of Object.keys(pool)) {
        if (pool[k].includes(sns)) {
            return k;
        }
    }
    return false
}

export function findTraceMatches(localPatients, tracePool) {
    let sns_col = -1;
    let traceName;
    const matches = {};
    Object.keys(localPatients).forEach(
        (k) => {
            matches[k] = []
        }
    )

    tracePool.forEach((row) => {
        if (sns_col < 0) {
            sns_col = findSNS(row);
            traceName = findName(row);
        } else {
            const sns = String(row[sns_col]).trim();
            const doctor = findInPool(sns, localPatients)
            if (doctor) {
                console.log(doctor)
                matches[doctor].push({ sns: sns, name: toTitleCase(row[traceName]) })
            }
        }
    });

    return matches;
}

export function toTitleCase(str) {
    return str.replace(
        /\w\S*/g,
        function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}

export function exportResults(results) {
    const patients = [];
    patients.push(["Médico", "Nome", "SNS"])
    Object.keys(results).forEach((k) => {
        patients.push(
            ...results[k].map((el) => ([k, el.name, el.sns]))
        )
    });

    const wb = XLSX.utils.book_new();

    /* Add the worksheet to the workbook */
    XLSX.utils.book_append_sheet(
        wb,
        XLSX.utils.aoa_to_sheet(patients),
        'Resultados'
    );
    XLSX.writeFile(wb, 'resultados.xlsx');
}