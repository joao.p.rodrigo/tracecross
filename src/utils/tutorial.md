# Tutorial

Para utilizar a ferramenta de cruzamento, irá precisar de 2 conjuntos de listas:

* Listas de utentes por médico de família
* Lista de utentes exportada do site Trace COVID

## Listas de utentes por Médico de Família

Navegue para o MIM@UF e introduza as suas credenciais;

Navegue na seguinte sequência (ilustrada nas imagens abaixo):

1. MIM@UF - Hierarquia dos locais 2015
2. P01. Inscritos
3. P01.01. Listagens
4. P01.01.L01. Inscritos > Utente

<p style="text-align:center">
    <img src="tutorial/1.png">
</p>
<p style="text-align:center">
    <img src="tutorial/2.png">
</p>
<p style="text-align:center">
    <img src="tutorial/3.png">
</p>
<p style="text-align:center">
    <img src="tutorial/4.png">
</p>

<hr />

* Escolha o mês (2020-03 ou 2020-02, caso o anterior não retornar resultados)
* Escolha a sua unidade da lista
* Carregue em **Executar Relatório**

<p style="text-align:center">
    <img src="tutorial/5.png">
</p>

<hr />

Quando o relatório for gerado, certifique-se que os seguintes campos têm o valor correcto:

* Unidade Funcional: a sua unidade
* Flag Médico de Família: C/ Médico de Família
* Médico de Família: um dos médicos que pertence à unidade
* Frequentador: Total
* Grupo Etário: Total

<p style="text-align:center">
    <img src="tutorial/6.png">
</p>

<hr />

* Prima o botão **Exportar**, assinalado na imagem:

<p style="text-align:center">
    <img src="tutorial/7.png">
</p>

<hr />

1. Seleccione **Arquivo em formato CVS**
2. Carregue em Exportar
3. O ficheiro será descarregado ou aparecerá uma janela para escolher onde guardar o ficheiro
4. **Altere o nome do ficheiro para o nome do médico de família ao qual pertencem os utentes** (isto facilitará a sua identificação mais tarde)

<p style="text-align:center">
    <img src="tutorial/8.png">
</p>

<hr />

#### Repita o processo para todos os Médicos da Unidade.

Para isso, basta voltar à janela dos resultados e alterar o nome do Médico.

#### Apenas precisa de obter estes ficheiros **uma vez**.

<hr />

## Lista de utentes exportada do site Trace COVID

* Navegue até ao website Trace COVID
* Entre na secção **Pessoas** (1)
* Carregue no botão **Exportar** (2), e guarde o ficheiro

<p style="text-align:center">
    <img src="tutorial/9.png">
</p>

<hr />

## Cruzar as listas

No site **TraceCross**, 

1. Em **Lista dos Médicos da Unidade**, carregue em **Carregar Ficheiros** (1),
2. Escolha todos os ficheiros das listas de utentes que retirou do MIM@UF;
3. Em **Lista Exportada do Trace COVID**, carregue em **Carregar Ficheiro (2);
4. Escolha o ficheiro que exportou do site Trace COVID (exemplo: *ExportPerson_PublicHealthUnit37_20200401154246*)

Os resultados por médico aparecerão na Tabela **Resultados**

<p style="text-align:center">
    <img src="tutorial/10.png">
</p>

