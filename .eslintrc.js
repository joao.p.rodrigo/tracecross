module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    "eslint:recommended"
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    "skipBlankLines": 0,
    "no-unused-vars": [
      1,
      {
        "args": "all",
        "argsIgnorePattern": "^_"
      }
    ],
    "vue/max-attributes-per-line": "off"
  }
}
